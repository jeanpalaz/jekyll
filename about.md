﻿---
layout: page
title: A PROPOS
permalink: /about/
---

Jean PALAZUELOS

Artisan logiciel

Membre de l’Agile Toulouse

Membre du Software Crafsmanship Toulouse depuis 2013

Signataire du [manifeste du Software Craftsmanship](http://manifesto.softwarecraftsmanship.org/) depuis 18/03/2015

* {% include icon-twitter.html username="@jeanpalaz" %}
* {% include icon-gitlab.html username="jeanpalaz" %}
* {% include icon-mastodon.html username="@jeanpalaz" %} Framapiaf/Mastodon
* {% include icon-diaspora.html username="4c0759d0b87e0134f37a2a0000053625" %} Framasphere/Diaspora

