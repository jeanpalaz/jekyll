---
layout: post
title: "Mémo GIT (2nd partie)"
date: 2017-04-26 00:42:00 +0000
comments: true
categories: [Blog, SVN, GIT, code, softwarecraftmanship, git-svn]
---
# GIT
Ce post est plutôt un mémo pour mon usage personnel, mais s'il peut aider quelqu'un j'en serai ravi :). 
Voilà donc un mémo traitant des problématiques ou cas d'utilisation que j'ai rencontré.
Pour mémoire, je rappelle que je suis dans un environnement SVN (centralisé) - GIT (en client sur mon poste).

## Cas d'utilisation
### Switch d'une branche SVN
Jusqu'à présent nous étions sur le 'master' côtès Git et en remote nous pointions sur 'origin/trunk', sur SVN il y a eu un problème en PROD et donc la nécessité de faire un patch dans une branche. 
La branche a été créée côtès SVN mais n'est pas visible avec Git:
```
$ git branch -r
  origin/V3.11_MAINT
  origin/V3.12_MAINT
  origin/tags/V3.11.1
  origin/tags/V3.12_Liv1
  origin/trunk
```
Il faut synchroniser

```
$ git svn fetch
blablabla assez long ...
 git branch -r
  origin/V3.11_MAINT
  origin/V3.12_MAINT
  origin/V3.13_MAINT
  origin/tags/V3.11.1
  origin/tags/V3.12_Liv1
  origin/trunk
```
et la magie la branche V3.13_MAINT apparait. Maintenant nous somme toujours sur le trunk en local:
```
$ git svn info
Path: .
URL: https://monserveur:8443/svn/MON_PROJET/trunk
Repository Root: https://monserveur:8443/svn/MON_PROJET
Repository UUID: 2a9a025e-1ade-4945-b3f3-c86421e8e34f
Revision: 2813
Node Kind: directory
Schedule: normal
Last Changed Author: m_lap
Last Changed Rev: 2813
Last Changed Date: 2017-04-18 11:32:21 +0200 (mar., 18 avr. 2017)
```
Il nous reste plus qu'a créé un branche dont l'origine est cette branche
```
$ git branch maint remotes/origin/V3.13_MAINT
$ git branch
  maint
* master
$ git checkout maint
Switched to branch 'maint'
$ git svn info
Path: .
URL: https://monserveur:8443/svn/MON_PROJET/branches/V3.13_MAINT
Repository Root: https://monserveur:8443/svn/MON_PROJET
Repository UUID: 2a9a025e-1ade-4945-b3f3-c86421e8e34f
Revision: 2814
Node Kind: directory
Schedule: normal
Last Changed Author: p_che
Last Changed Rev: 2808
Last Changed Date: 2017-04-12 15:20:33 +0200 (mer., 12 avr. 2017)
```
Et nous pouvons retouver notre flux de travail habituel! \o/

### Erreur 
* Au cours de mes devs j'ai oublié parfois d'ajouter une modification, un paramètre ... un dernier petit détails, pour y remédier je fais:<br>
  `git commit --ammend`
* Puis une fois terminé je pousse sur SVN
  `git svn dcommit `
* Et puis un jour dans ma lancé, je refais un `git commit --ammmend` sur un commit déjà poussé sur SVN, il faut donc faire un roolback et un nouveau commit puis pousser, soit:
```
git reflog
git reset --soft HEAD@{1}
git commit -C HEAD@{1}
git svn dcommit
```
* Dans la folie du commit avec ammend , j'ai aussi essayé un revert qui m'a mis dans cette position : HEAD/master, et origin/trunk décalé alors que je voulais garder la version d'origine
 
```
$ git log --all --graph --color --name-status --format='%C(yellow)%h%Creset %cr %C(blue)%cn%Creset -%C(auto)%d%Creset %s'
 * 95c473c 27 hours ago j_pal - (HEAD -> master) A second fix
|
| M     PROJECT/Fixes/Restore/restore.ksh
| M     PROJECT/Fixes/Restore/README.txt
* 95c473c 27 hours ago j_pal - (origin/trunk) My Fix 
|
| M     PROJECT/Fixes/Restore/restore.ksh
| M     PROJECT/Fixes/Restore/README.txt
* ed5f1f3 2 days ago j_pal - Restore script
|
| A     PROJECT/Fixes/Restore/restore.ksh
| A     PROJECT/Fixes/Restore/README.txt
|
...
```
* Il fallait donc revenir à un commit ou Head, master et origin pointé sur le même commit:
```
git reset --hard origin/trunk
```

## Questions
### Pourquoi `git remote -v` ne me renvoie rien?
* Je ne sais si vous savez contacter moi...

------------------ 
# /!\ REGLES A RESPECTER - RAPPEL
* _git svn_ NE PAS partager/interrargir avec un autre repo git, toujours repasser par le repo SVN 
* Garder son _historique_ le plus _linéaire possible_ (rebase est ton ami)
 

------------------
# Liens et sources
* [Pro Git](https://git-scm.com/book/fr/v2)
* [8.1 Git et les autres systèmes - Git et Subversion](https://git-scm.com/book/fr/v1/Git-et-les-autres-syst%C3%A8mes-Git-et-Subversion)
* [git-svn(1) Manual Page ](https://www.kernel.org/pub/software/scm/git/docs/git-svn.html#_configuration)
* [Git and Subversion](http://www.tfnico.com/presentations/git-and-subversion)
