---
layout: post
title: "Premier DevFest à Toulouse"
date: 2016-11-03 18:22:48 +0300
comments: true
categories: [Blog, softwarecraftmanship]
---

Le premier Google Dev Fest de Toulouse a eu lieu à l'IUT de Blagnac.
A ma connaissance le premier grand événement sur toulouse par et pour les développeurs, et ça c'est cool !

Voici un petit compte rendu de mon parcours:

## Keynote d'ouverture : Évolution du métier de développeur ces dernières années. par Alex Danvy de MS France.
Un peu d'histoire sur le métier de développeur: le travail et la vision des gens sur ce travail, et puis le présent et l'avenir du métier.
Pour résumé je dirais en phase avec la vision du mouvement "Artisan développeur" (Software Craftmanship).

## Clean Code par Antoine Vernois
Deja vu et revu mais je m'en lasse jamais. Tellement de bonnes choses à dire sur la façon de bien coder. Si déja tout le monde pouvait s'y mettre sur les pratiques de bases, on pourrait se concentrer sur des choses plus essentielles.

## Dans les coulisses de Google BigQuery par Aurélie Vache.
[http://fr.slideshare.net/aurelievache/dans-les-coulisses-de-google-bigquery]
Découverte pour moi d'une partie de l'écosystème Google, de l'historique du BigData. On a pu voir des requètes s'exécuter sur des tables de 10000 colonnes avec une dizaine de millions de lignes en moins de 2 secondes par réseau, j'en suis resté bouche bée !

## Client REST en Java: trop facile avec Feign ! par Igor Laborie
[https://github.com/OpenFeign/feign]
[https://github.com/ilaborie/demo-feign/]
Du live coding ! c'est toujours beau et bon à voir mais faut pas cligner des yeux au risque de perdre le fil. Mais effectivement comme le dit le titre c'est facile et rapide à mettre en place. Avec en bonus du Java 9 !

## A table ! 
Bon buffet mais la précédente prez ayant débordée, on a eu du mal à trouver à manger ...

## Les Acteurs, un modèle pour dompter le parallélisme par Frédéric Cabestre.
Super historique sur le sujet, ca m'a rappelé mes cours de fac sur les systèmes. Tout cela pour dire qu'on a pas beaucoup inventé de nouvelles choses mais maintenant ça devient du courrant dans nos machines ... Et puis redécouverte du modèle Acteurs.

## Au secours, ma prod est sous Docker ! par François Teychene
Très bon REX sur le passage de l'utilisation de Docker en dev+int vers la prod avec ses pièges ! La relecture des slides s'impose tellement le débit était élevé et dense...

## Android et programmation réactive - RxJava par Elisabel Généreux
Pour ma part cela a été une introduction à la prog sous Android avec l'utilsation de JavaRx. JavaRx vu le matin même avec la prez D'Igor, donc c'était dans la continuité mais pour Android.

## My resumé in an operating system : un buzz et ses conséquences par Mathieu Passenaud
Prez plutôt décalée et originale sur la naissance d'un buzz, les conséquences sur l'autohébergement : sympathique et parfois flippant ! 

Voilà : je n'ai pas de retour sur les autres prez mais le niveau avait l'air sans aucun doute excellent !

Vivement l'année prochaine pour une autre édition, et bravo aux organisateurs !

**Happy Coding !**

[devfesttoulouse.fr](https://devfesttoulouse.fr/)

