---
layout: post
title: "Objets Immuables en Java memo"
date: 2017-04-12 00:42:00 +0000
categories: [code, Java, immutable]
--- 

Après la lecture de cette article [Immutable Objects in Java](https://dzone.com/articles/immutable-objects-in-java), je me suis fait un petit memo sous forme de mindmap.
Je partage cette carte. 

![Immutable Objects Java Mindmap](/assets/ImmutableObjectsJava.png)

Happy Coding !