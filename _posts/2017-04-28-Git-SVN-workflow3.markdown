---
layout: post
title: "Mémo GIT (3ième partie)"
date: 2017-04-28 00:42:00 +0000
comments: true
categories: [Blog, SVN, GIT, code, softwarecraftmanship, git-svn, merge, rebase]
---

Ce post est plutôt un mémo pour mon usage personnel, mais s'il peut aider quelqu'un j'en serai ravi :). 
Voilà donc un mémo traitant des problématiques ou cas d'utilisation que j'ai rencontré.
Pour mémoire, je rappelle que je suis dans un environnement SVN (centralisé) - GIT (en client sur mon poste). Cf. les articles précédents: (https://jeanpalaz.gitlab.io/blog/2017/02/01/Git-SVN-workflow/) (https://jeanpalaz.gitlab.io/blog/2017/04/26/Git-SVN-workflow2/)

## Cas d'utilisation
### Fusion du trunk et d'une branche SVN avec conflit
Il y a 2 façons de fusionner avec GIT avec la commande `git merge` ou `git rebase`. Je vous laisse le soin de regarder la doc afin de vous familiariser ou d'appronfondir la question. Il y a une chose à retenir c'est 
`Ne rebasez jamais des commits qui ont déjà été poussés sur un dépôt public.` -- Progit : Les dangers du rebasage.
Si je reprends le cas de du [Memo2](https://jeanpalaz.gitlab.io/blog/2017/04/26/Git-SVN-workflow2/), nous sommes dans le cas ou nous avons une branche publique SVN (maint) et une branche publique (trunk). Donc nous allons utiliser le `git merge`.
```
j_pal MINGW64 /c/dev/workspace (master)
$ git merge maint
Auto-merging PROJECT/xsl/eipc.xsl
Auto-merging PROJECT/xsl/common/sblist.xsl
CONFLICT (content): Merge conflict in PROJECT/xsl/common/sblist.xsl
Automatic merge failed; fix conflicts and then commit the result.

j_pal MINGW64 /c/dev/workspace (master|MERGING)
```
Paf ! C'est le drame conflit !!! Don't Panic ! On édite le fichier on résout le conflit classiquement. On sauve. Et on ajoute le fichier à l'index `git add  monFichier`, c'est de cette façon que l'on résout les conflits sous GIT. Le reste devient une habitude :)
```
$ git commit -m "Fusssiiiiiioonnn"
[master c341143] Fusssiiiiiioonnn
j_pal MINGW64 /c/dev/workspace (master)
git svn dcommit
....
j_pal MINGW64 /c/dev/workspace (master)
git branch -d maint
Deleted branch maint (was 1e474ce).

```
Au passage un remarque la mention de MERGING dans le prompt qui disparait après commit.

------------------ 
# /!\ REGLES A RESPECTER - RAPPEL
* _git svn_ NE PAS partager/interrargir avec un autre repo git, toujours repasser par le repo SVN 
* Garder son _historique_ le plus _linéaire possible_ (rebase est ton ami)
* Ne rebasez jamais des commits qui ont déjà été poussés sur un dépôt public.
 

------------------
# Liens et sources
* [Pro Git](https://git-scm.com/book/fr/v2)
* [git merge](https://git-scm.com/docs/git-merge)
* [git rebase](https://git-scm.com/docs/git-rebase)
