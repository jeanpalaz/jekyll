---
layout: post
title:  "Welcome back!"
date:   2018-10-08 00:42:00 -0300
categories: blog
---

Et voilà, il m'a fallut du temps mais le blog est de retour. Qu'est ce qui m'a bloqué pendant cette année passée ...

Et bien une mise à jour de Gitlab a fait que le gitlab-ci ne déployait plus les pages ...

Il m'a fallut du temps et de l'énergie pour trouver! Oui 1 an c'est long mais par morceau de 30min parci parlà puis les coupures et la remise en route ou il fallait parfois reprendre tout depuis le début ... En parallèle avec d'autres activités et de plus avec un priorité basse ...

Mais récemment un entretien m'a permis de me remotiver sur le sujet. J'ai donc appliqué le **STOP starting START finishing**, sur ce sujet. Un oeil neuf et une reprise à zéro du problème, m'a permis de comprendre que suite à une mise à jour de Gitlab, le projet path du projet avait été réinitialisé à mon insu. Il a fallut donc juste modifier dans les "advanced settings" le path du repository comme attendu pour les Gitlab-pages.
Pensant que cela venait d'octopress, j'avais aussi basculé sur Jekkyl, et hop une façon de plus d'apprendre quelque chose !


Voila je peaufine quelques pages header / footer ... mais je peux republier mes revue de presse et d'autres choses ...

