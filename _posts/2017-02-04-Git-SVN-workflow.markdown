---
layout: post
title: "Mémo GIT"
date: 2017-02-01 00:42:00 +0000
comments: true
categories: [Blog, SVN, GIT, code, softwarecraftmanship]
---
# GIT
Ce post est plutôt un mémo pour mon usage personnel, mais s'il peut aider quelqu'un j'en serai ravi :). 
Voilà donc un mémo traitant des cas de base. Etant encore dans un environnement SVN, j'en avais marre de ne pas être sous Git, alors une façon de reprendre le contrôle c'est d'utiliser Git comme client SVN.
Oui moi aussi je veux faire du [Hype Driven Developement](https://blog.daftcode.pl/hype-driven-development-3469fc2e9b22#.nituglhbl) ;-) ... Git est maintenant incontournable et bien adapté aux méthodes du moment.

--------------------------------------------
## Vocabulaire
Suivis      /     Tracked<br/>
Non suivis  /     Untracked<br/>
Instantané  /     Snapshot<br/>

Validé      /     Committed -> Validé signifie que les données sont stockées en sécurité dans votre base de données locale<br/>
Modifié     /     Modified  -> Modifié signifie que vous avez modifié le fichier mais qu’il n’a pas encore été validé en base.<br/>
Indexé      /     Staged    -> indexé signifie que vous avez marqué un fichier modifié dans sa version actuelle pour qu’il fasse partie du prochain instantané du projet<br/>
Inchangé    /     unmodified<br/>

--------------------------------------------
## Comment utiliser Git au quotidien 
### Suivre des fichiers (ajouter à l'index)
  `git add monFic`<br/>
  Supprimé la track (de l'indexe)<br/>
    `git reset HEAD monFic`
    
### Validé 
  `git commit -m "blabla"`<br/>
  Modifier le message du dernier commit 
  `git commit --amend -m "mon nouveau message"`<br/>
  Mauvais commit oublié un fichier, ajouter supprimer etc ... puis<br/>
  `git commit --amend`
  /!\ "Ne modifiez pas votre dernière validation si vous l’avez déjà publiée !" progit.fr

## BRANCHE
### Créer une branche
  `git branch maBranche`<br/>
  `git checkout maBranche`<br/>
  En 1 seule commande : `git checkout -b maBranche`
  
### Changer de branche (switch)
  `git checkout maBranche`<br/>

### Supprimer une branche 
  `git branche -d maBranche`<br/>

### voir les logs
  `git log --decorate --all --graph`

--------------------------------------------
# Comment utiliser git dans un environment SVN (CLI)
## Les commandes à connaitre
* Cloner un projet existant svn -> git<br/>
`git svn clone -s [urlsvn] -r revisionYaPasLongtemps:HEAD`<br/>
  revisionYaPasLongtemps : par exemple la revision du debut de version courante. PAS tout le projet depuis sa création !

* Démarrer une nouvelle tâche dans une branche:<br/>
`git checkout -b maTache`

* Faire ses dev puis git add, git commit etc ...  cf. l'utilisation conrrante de Git<br/>
  --->/!\ GIT en local, pas de partage avec d'autre GIT: passer par SVN!

* Mettre à jour son environnement Server SVN -> Repo local: sur master<br/>
`git svn rebase`

* Committer ses modifs git -> svn: <br/>
`git svn dcommit`
 
## exemple de workflow:
    git checkout -b maTache    // création de la branche pour le dev à faire et basculement sur celle ci
    // dev
    git add monFichier          // ajouter au tracking
    // dev
    git add monFichier          // remettre dans la zone d'indexe car modifié
    git commit -m "message"     // Commit sur la branche
    git branch -a               // pour vérifier sur quelle branche on est
    git checkout master         // basculer sur la branche master
    git svn rebase              // pour synchro avant merge
    git merge maBranche         // merge de maBranche sur master
    git log --decorate --all --graph // on peut verifier le graph et que tout est linéaire
    git svn dcommit
    git branch -d maTache       // la branche de travail terminée est mergé sur master on peut la supprimer

------------------ 
# /!\ REGLES A RESPECTER
* _git svn_ NE PAS partager/interrargir avec un autre repo git, toujours repasser par le repo SVN 
* Garder son _historique_ le plus _linéaire possible_ (rebase est ton ami)
 
## Dans eclipse ?
* EGit ne prend pas en charge git svn ...
* La ligne de commande est ton ami :)
* Les autres éditeurs ... je ne sais pas ...

------------------
# Liens et sources
* Voici le premier lien qui m'a vraiment servi à démarrer : [Enfin comprendre git](https://www.miximum.fr/blog/enfin-comprendre-git/)
* Plus d'informations très claires du même auteur : [Git Rebase](https://www.miximum.fr/blog/git-rebase/)
* [Pro Git](https://git-scm.com/book/fr/v2)
* [Git log](http://blog.xebia.fr/2015/11/13/git-essentials-1-log/)
* [From SVN to Ggit](http://blog.loof.fr/2010/08/from-svn-to-git.html)
