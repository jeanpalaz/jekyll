---
layout: post
title: "Mes butinages de la semaine 22"
date: 2017-06-06 00:42:00 +0000
comments: false
categories: [agilité, vie-privée, softwarecraftmanship, faille]
---

**Bon butinage !**

## Artisanat logiciel
* [The Start-Up Trap](http://blog.cleancoder.com/uncle-bob/2013/03/05/TheStartUpTrap.html)

## Vie privée
* [Addictions en série](https://framablog.org/2017/05/23/addictions-en-serie/)
* [Le téléphone portable, c’est le rêve de Staline devenu réalité](https://usbeketrica.com/article/le-telephone-portable-c-est-le-reve-de-staline-devenu-realite)

## Podcast
* [Ce que l’on doit à Aaron Swartz](https://www.franceculture.fr/emissions/les-nouvelles-vagues/regler-nos-dettes-3-ce-que-lon-doit-aaron-swartz)

## Agile
* [Les Lego au boulot ou l’art de battre son patron à Chifoumi !](https://www.synbioz.com/blog/Les_LEGO_au_boulot_ou_lart_de_battre_son_patron_a_chifoumi)

## Divers
* [Les pacemakers sont criblés de failles de sécurité](http://www.01net.com/actualites/les-pacemakers-sont-cribles-de-failles-de-securite-1175162.html)