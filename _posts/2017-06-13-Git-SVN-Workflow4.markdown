---
layout: post
title: "Mémo GIT (4ième partie)"
date: 2017-06-13 00:42:00 +0000
comments: true
categories: [SVN, GIT, code, softwarecraftmanship, git-svn, cherry-pick]
---

Ce post est plutôt un mémo pour mon usage personnel, mais s'il peut aider quelqu'un j'en serai ravi :). 
Voilà donc un mémo traitant des problématiques ou cas d'utilisation que j'ai rencontré.
Pour mémoire, je rappelle que je suis dans un environnement SVN (centralisé) - GIT (en client sur mon poste). Cf. les articles précédents: [mémo1](https://jeanpalaz.gitlab.io/blog/2017/02/01/Git-SVN-workflow/), [mémo2](https://jeanpalaz.gitlab.io/blog/2017/04/26/Git-SVN-workflow2/), [mémo3](https://jeanpalaz.gitlab.io/blog/2017/05/01/Git-SVN-workflow3/)

## Cas d'utilisation
### Fusion d'un commit d'un branche vers une autre
Au cours de la vie des projets, il est courrant de faire une branche de maintenance pour faire évoluer le code d'une version déja officialisée. Ensuite, il est possible ([cf. épisode précédent](https://jeanpalaz.gitlab.io/blog/2017/05/01/Git-SVN-workflow3/)) de faire un merge (ou rebase) de cette branche sur la branche principale (master ou trunk) afin de récupérer les corrections. Or il arrive parfois que sur la branche principale, des 'refactors' aient été faits rendant certains commits de la branche de maintenance obsolètes. Donc il ne faut pas les reporter sur la branche principale.
Il faut donc faire une selection des commits voulus et les appliquer sur la branche principale. La commande Git magique est le _cherry-pick_:
Workflow:

- Sur la branche de maintenance identifier à l'aide de `git log` les commits (relevé le SHA-1 associé)

- Sur la branche principale appliqué les commits

```
bash workflow
j_pal MINGW64 /c/dev/workspace/PROJECT (master)
$ git svn rebase
Current branch master is up to date.

j_pal MINGW64 /c/dev/workspace/PROJECT (master)
$ git checkout maint
Switched to branch 'maint'

j_pal MINGW64 /c/dev/workspace/PROJECT/AWN_PACAT_MNT/environment (maint)
$ git logAll
 ...
 * d3e0611 2 days ago j_pal - QC3971-lake of Consult client config
| |
| | M   PROJECT/environment/service-compile.xml
| * 15716c9 2 days ago j_pal - QC3970-NEO manuals prod error
| |
| | M   PROJECT/environment/service-build.xml
| *   f2a0ad3 3 days ago j_pal - OTH-Maintenance V3.13
 ...

j_pal MINGW64 /c/dev/workspace/PROJECT/AWN_PACAT_MNT/environment (master)
$ git cherry-pick  15716c9
[master 81e3a58] QC3970-NEO manuals prod error
 Author: j_pal <j_pal@2a9a025e-1ade-4945-b3f3-c86421e8e34f>
 Date: Wed May 31 13:19:09 2017 +0000
 1 file changed, 4 insertions(+), 4 deletions(-)

j_pal MINGW64 /c/dev/workspace/PROJECT (master)
$ git svn dcommit
Committing to https://airplfms15.sesame.com:8443/svn/PROJECT/trunk ...
        M       PROJECT/AWN_PACAT_MNT/environment/service-build.xml
Committed r2857
        M       PROJECT/AWN_PACAT_MNT/environment/service-build.xml
r2857 = 91fccb894cab4030038b010f7d1b1e1be5b45a43 (refs/remotes/origin/trunk)
No changes between 81e3a58e3f624469eb38690761d819d299b95086 and refs/remotes/origin/trunk
Resetting to the latest refs/remotes/origin/trunk
```

Tout est en ordre ! En cas de conflit c'est comme d'habitude [mémo3](https://jeanpalaz.gitlab.io/blog/2017/04/28/Git-SVN-workflow3/)
### Note: 
_git logAll_ est un alias inséré dans ma config de cette façon:
`git config alias.logall=log --all --graph --color --name-status --format='%C(yellow)%h%Creset %cr %C(blue)%cn%Creset -%C(auto)%d%Creset %s'`

------------------ 
# /!\ REGLES A RESPECTER - RAPPEL
* _git svn_ NE PAS partager/interrargir avec un autre repo git, toujours repasser par le repo SVN 
* Garder son _historique_ le plus _linéaire possible_ (rebase est ton ami)
* Ne rebasez jamais des commits qui ont déjà été poussés sur un dépôt public.`
 

------------------
# Liens et sources
* [Pro Git](https://git-scm.com/book/fr/v2)
* [GESTION PAR REBASAGE ET SÉLECTION DE COMMIT](https://git-scm.com/docs/git-cherry-pick)
