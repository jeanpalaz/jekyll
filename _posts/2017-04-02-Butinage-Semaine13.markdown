---
layout: post
title: "Mes butinages de la semaine 13"
date: 2017-04-02 00:42:00 +0000
comments: true
categories: [agilité, liberté, softwarecraftmanship]
---

**Bon butinage !**

## Artisanat logiciel
* [La qualité dans le logiciel : partie 1, la qualité perçue](https://medium.com/arpinum/la-qualit%C3%A9-dans-le-logiciel-partie-1-la-qualit%C3%A9-per%C3%A7ue-3a889b1ef243#.b0zr8q9m5)
* [Dévelop-PEURS ](https://shirleyalmosni.wordpress.com/2017/03/31/develop-peurs/)

## Logiciel libre
* [L'épopée de Firefox OS](https://blog.mozfr.org/post/2017/03/epopee-Firefox-OS)
* [Quand les fermiers américains sont obligés de pirater leurs propres tracteurs pour pouvoir les réparer](http://www.20minutes.fr/high-tech/2036523-20170323-quand-fermiers-americains-obliges-pirater-propres-tracteurs-pouvoir-reparer)
* [Comment j’ai mis fin à un chantage logiciel](https://linuxfr.org/users/freejeff/journaux/comment-j-ai-mis-fin-a-un-chantage-logiciel)

## Sécurité, vie privée
* [Google, nouvel avatar du capitalisme, celui de la surveillance](https://framablog.org/2017/03/28/google-nouvel-avatar-du-capitalisme-celui-de-la-surveillance/)
* [Cette menace vise les utilisateurs de GitHub](http://homputersecurity.com/2017/03/30/cette-menace-vise-les-utilisateurs-de-github/)
* [Le Congrès américain abandonne les règles de vie privée dans les télécoms](https://www.nextinpact.com/news/103846-le-congres-americain-abandonne-regles-vie-privee-dans-telecoms.htm)

## Éducation
* [Philippe Garric - Éduquer sans violence (Valérie Orvain)](https://youtu.be/Hk-fk8gI2z0)
