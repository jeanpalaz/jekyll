---
layout: post
title: "Création des GitLab Pages"
date: 2016-06-06 18:22:48 +0300
comments: true
categories: [Blog, GitLab, Pages]
---

Bienvenue sur mon Blog !

En tant qu'artisan logiciel, je me devais de faire un blog, voilà c'est chose faite!
Premier apprentissage de Gitlab et des Gitlab Pages, de la génération de pages statiques avec Jekyll.

Ce blog est hébergé par [GitLab.com][] et déployé avec [GitLab Pages][pages].

Plus d'informations sur <https://gitlab.com/pages/octopress>.

Happy Coding!

[GitLab.com]: https://about.gitlab.com/gitlab-com
[pages]: https://pages.gitlab.io
