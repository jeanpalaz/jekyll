---
layout: post
title: "Mes butinages de la semaine 15"
date: 2017-04-17 00:42:00 +0000
comments: false
categories: [agilité, liberté, softwarecraftmanship, Immutable, Ubuntu]
---

**Bon butinage !**

## Artisanat logiciel
* [Immutable Objects in Java](https://dzone.com/articles/immutable-objects-in-java)

## Sécurité, vie privée
* [Réseau Mastodon : à lire avant d'ouvrir votre instance](https://www.taneleo.fr/reseau-mastodon-a-lire-avant-d-ouvrir-votre-instance)
* [Adieu Darknet, bonjour Librenet](https://www.franceculture.fr/emissions/la-methode-scientifique/adieu-darknet-bonjour-librenet)
* [Chiffrement : le rétropédalage de l'équipe d'Emmanuel Macron ](http://www.01net.com/actualites/chiffrement-les-propositions-de-macron-contestees-par-les-experts-en-securite-informatique-1140401.html)

## Divers
* [Growing Ubuntu for cloud and IoT, rather than phone and convergence](https://insights.ubuntu.com/2017/04/05/growing-ubuntu-for-cloud-and-iot-rather-than-phone-and-convergence/)
* [Les liens d’attachement : s’en libérer !](https://fannys.fr/les-liens-dattachement/)